package com.example.manolis.freagmenttry;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class GameActivity extends Activity {
    Context context = this;

    public static String TAG = "TzinoMainActivityTAG";
    public String redColorString = "#CC0000";

    public int WR = 0;
    public int EL = 1;
    public int SH = 2;
    public int CL = 3;

    public int CORD_X = 0;
    public int CORD_Y = 1;
    public int CORD_Z = 2;

    MediaPlayer mp;
    MediaPlayer mp_error;

    private BluetoothManager mBluetoothManager;
    private BluetoothGattServer mBluetoothGattServer;
    private BluetoothLeAdvertiser mBluetoothLeAdvertiser;

    // Replacement for xml usage
    private Set<BluetoothDevice> mRegisteredDevices = new HashSet<>();

    public float[][] joints = new float[4][3]; // 4 joints. each has 3 floats for its coordinates

    private int connection_state = BluetoothProfile.STATE_DISCONNECTED;

    public LinearLayout showreps_text;
    public TextView did_or_f_ex_1_text, press_start_to_text, reps_num, down_up_down_txt; // all the text
    public ImageButton img1, img2; // left and right images
    public ImageButton picked_image;
    public Button restartbt, startbt; // restart and start button
    public int reps = 0; // repetitions of exercise the patient has done (obviously start at 0)
    public boolean an_image_is_selected = false;
    public boolean started = false;
    public enum Classif {
        RIGHT,
        DOWN,
        OTHER,
        NONE
    }
    Classif arm_classification = Classif.NONE;
    int step = 0;

    public void onclickfunction(View view) {
        switch (view.getId()) {
            case R.id.imageButton: // case of clicking image 1
                if (!an_image_is_selected && !started) {
                    an_image_is_selected = true;

                    img1.setVisibility(View.VISIBLE);
                    img2.setVisibility(View.GONE);
                    findViewById(R.id.emptyhorizontalspace).setVisibility(View.GONE);

                    press_start_to_text.setVisibility(View.VISIBLE);

                    restartbt.setVisibility(View.VISIBLE);
                    startbt.setVisibility(View.VISIBLE);

                    picked_image = findViewById(R.id.imageButton);
                }
                break;

            case R.id.imageButton2: // case of clicking image 2
                if (!an_image_is_selected && !started) {
                    an_image_is_selected = true;

                    img2.setVisibility(View.VISIBLE);
                    img1.setVisibility(View.GONE);
                    findViewById(R.id.emptyhorizontalspace).setVisibility(View.GONE);

                    press_start_to_text.setVisibility(View.VISIBLE);

                    restartbt.setVisibility(View.VISIBLE);
                    startbt.setVisibility(View.VISIBLE);

                    picked_image = findViewById(R.id.imageButton2);
                }
                break;

            case R.id.button: // case of clicking "RESTART" button
                reps = 0;
                step = 0;
                started = false;
                an_image_is_selected = false;

                img1.setVisibility(View.VISIBLE);
                findViewById(R.id.emptyhorizontalspace).setVisibility(View.VISIBLE);
                img2.setVisibility(View.VISIBLE);

                showreps_text.setVisibility(View.INVISIBLE);
                down_up_down_txt.setVisibility(View.INVISIBLE);

                startbt.setText("START");
                startbt.setVisibility(View.INVISIBLE);
                restartbt.setVisibility(View.INVISIBLE);
                did_or_f_ex_1_text.setTextColor(Color.BLACK);
                did_or_f_ex_1_text.setVisibility(View.INVISIBLE);
                press_start_to_text.setTextColor(Color.BLACK);
                press_start_to_text.setVisibility(View.GONE);

                img1.setImageResource(R.drawable.tpose);
                img2.setImageResource(R.drawable.tpose2);

                reps_num.setText("" + reps);

                picked_image = null;
                break;

            case R.id.button3: // case of clicking "START" button
                if (an_image_is_selected) {
                    press_start_to_text.setVisibility(View.GONE);
                    did_or_f_ex_1_text.setTextColor(Color.BLACK);
                    did_or_f_ex_1_text.setText("⚠ Haven't connected with MRC yet...");
                    did_or_f_ex_1_text.setVisibility(View.VISIBLE);

                    showreps_text.setVisibility(View.VISIBLE);
                    down_up_down_txt.setVisibility(View.VISIBLE);


                    startbt.setVisibility(View.INVISIBLE);

                    if (startbt.getText().equals("START")) {
                        started = true;
                    }
                    reps = 0;
                    reps_num.setText("" + reps);
                }
                break;
        }
    }

    /* -------------------------------------------------------------------------------- Bluetooth functionality functions -------------------------------------------------------------------------------- */

    /**
     * Listens for Bluetooth adapter events to enable/disable
     * advertising and server functionality.
     */
    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.debug("BroadcastReceiver just received some context and some intent...");
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF);
            switch (state) {
                case BluetoothAdapter.STATE_ON:
                    Log.debug("\tstate was ON. Starting both advertising and server...");
                    startAdvertising();
                    startServer();
                    break;
                case BluetoothAdapter.STATE_OFF:
                    Log.debug("\tstate was OFF. Stopping both server and advertising...");
                    stopServer();
                    stopAdvertising();
                    break;
                default:
                    // Do nothing
            }
        }
    };

    /**
     * Begin advertising over Bluetooth that this device is connectable
     * and supports the Current Time Service.
     */
    private void startAdvertising() {
        Log.debug("Starting advertising...");
        BluetoothAdapter bluetoothAdapter = mBluetoothManager.getAdapter();
        mBluetoothLeAdvertiser = bluetoothAdapter.getBluetoothLeAdvertiser();
        if (mBluetoothLeAdvertiser == null) {
            Log.warn("Failed to create advertiser");
            return;
        }
        Log.debug("\tsetting settings...");
        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setConnectable(true)
                .setTimeout(0)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                .build();

        Log.debug("\tsetting advertise data as this...");
        AdvertiseData data = new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .setIncludeTxPowerLevel(true)
                .addServiceUuid(new ParcelUuid(Profile.JointsServiceUUID))
                .build();
        Log.debug("advertise data: " + data.toString());

        mBluetoothLeAdvertiser.startAdvertising(settings, data, mAdvertiseCallback);
        Log.debug("\tadvertising has started!...");
    }

    /**
     * Stop Bluetooth advertisements.
     */
    private void stopAdvertising() {
        Log.debug("Stopping advertising...");
        if (mBluetoothLeAdvertiser == null) {
            Log.debug("\tbleAdvertiser was already null. Nothing to do...");
            return;
        }
        mBluetoothLeAdvertiser.stopAdvertising(mAdvertiseCallback);
        Log.debug("\tadvertising has stopped!...");
    }

    /**
     * Initialize the GATT server instance with the services/characteristics
     * from the Time Profile.
     */
    private void startServer() {
        Log.debug("Openning gatt server");
        mBluetoothGattServer = mBluetoothManager.openGattServer(this, mGattServerCallback);
        if (mBluetoothGattServer == null) {
            Log.warn("Unable to create GATT server");
            return;
        }

        Log.debug("\tadding joints service...");
        mBluetoothGattServer.addService(Profile.customJointsService());
        Log.debug("\tserver has started!...");
    }

    /**
     * Shut down the GATT server.
     */
    private void stopServer() {
        Log.debug("Stopping server...");
        if (mBluetoothGattServer == null) {
            Log.debug("\tbluetooth gatt server was already null. nothing to do...");
            return;
        }

        mBluetoothGattServer.close();
        Log.debug("\tserver has stopped");
    }

    /**
     * Callback to receive information about the advertisement process.
     */
    private AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            Log.debug("LE Advertise Succeeded. (this message is from callback)...");
        }

        @Override
        public void onStartFailure(int errorCode) {
            Log.warn("LE Advertise Failed: "+ errorCode + "(this message is from callback)...");
        }
    };


    /* -------------------------------------------------------------------------------- Gatt server callback -------------------------------------------------------------------------------- */
    /**
     * Callback to handle incoming requests to the GATT server.
     * All read/write requests for characteristics and descriptors are handled here.
     */

    private float norm_3d(float x, float y, float z){
        return (float)Math.sqrt((double)(x*x + y*y + z*z));
    }

    private float norm_xy_proj(float x, float y){
        return (float)Math.sqrt((double)(x*x + y*y));
    }

    private boolean user_facing_camera(){
        return (norm_xy_proj(joints[SH][CORD_X] - joints[CL][CORD_X], joints[SH][CORD_Y] - joints[CL][CORD_Y])/norm_3d(joints[SH][CORD_X] - joints[CL][CORD_X], joints[SH][CORD_Y] - joints[CL][CORD_Y], joints[SH][CORD_Z] - joints[CL][CORD_Z]) > 0.93);
    }

    private boolean arm_is_straight(){
        float SW_len = norm_3d(joints[SH][CORD_X] - joints[WR][CORD_X], joints[SH][CORD_Y] - joints[WR][CORD_Y], joints[SH][CORD_Z] - joints[WR][CORD_Z]);
        float SE_len = norm_3d(joints[SH][CORD_X] - joints[EL][CORD_X], joints[SH][CORD_Y] - joints[EL][CORD_Y], joints[SH][CORD_Z] - joints[EL][CORD_Z]);
        float EW_len = norm_3d(joints[WR][CORD_X] - joints[EL][CORD_X], joints[WR][CORD_Y] - joints[EL][CORD_Y], joints[WR][CORD_Z] - joints[EL][CORD_Z]);
        return (SW_len/(SE_len + EW_len) > 0.9);
    }

    private boolean arm_down(){
        return (Math.abs(joints[SH][CORD_X] - joints[EL][CORD_X]) < 0.08 &&
                Math.abs(joints[WR][CORD_X] - joints[EL][CORD_X]) < 0.08 &&
                Math.abs(joints[SH][CORD_Z] - joints[EL][CORD_Z]) < 0.12 &&
                joints[SH][CORD_Y] < joints[EL][CORD_Y] &&
                joints[EL][CORD_Y] < joints[WR][CORD_Y]);
    }

    private boolean arm_right(){
        return (Math.abs(joints[SH][CORD_Y] - joints[EL][CORD_Y]) < 0.08 &&
                Math.abs(joints[EL][CORD_Y] - joints[WR][CORD_Y]) < 0.08 &&
                Math.abs(joints[SH][CORD_Z] - joints[WR][CORD_Z]) < 0.12 &&
                joints[SH][CORD_X] < joints[EL][CORD_X] &&
                joints[EL][CORD_X] < joints[WR][CORD_X]);
    }

    private BluetoothGattServerCallback mGattServerCallback = new BluetoothGattServerCallback() {

        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            Log.debug("gatt server onConnectionStateChanged! (message from gatt server callback)...");
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                connection_state = newState;
                Log.debug("\tBluetoothDevice CONNECTED: " + device);
                Log.debug("\tsince a bluetooth device connected, will stop advertising...");
                stopAdvertising();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.debug("\tsince a bluetoooth device dc'd, will start advertising...");
                startAdvertising();
                connection_state = newState;
                Log.debug("\tBluetoothDevice DISCONNECTED: " + device + " -Status: " + status);

                //Remove device from any active subscriptions
                mRegisteredDevices.remove(device);
            } else  {
                Log.debug( "\tBluetooth Device: Other connection state change: " );
            }
        }

        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device,
                                                 int requestId,
                                                 BluetoothGattCharacteristic characteristic,
                                                 boolean preparedWrite,
                                                 boolean responseNeeded,
                                                 int offset,
                                                 byte[] value) {

            /*
             * STATISTICS:
             * OUT OF 700 PACKETS MEASURED, IT WAS CONCLUDED THAT ON AVERAGE 16.09 PACKETS ARE SENT
             * EACH SECOND (62.1442ms delay between packets received)
             */

            if (Profile.arm.equals(characteristic.getUuid())) {
                for (int i = 0; i < 4; i++) {
                    byte[] actual_data = new byte[4];
                    actual_data[0] = value[4*i + 3];
                    actual_data[1] = value[4*i + 2];
                    actual_data[2] = value[4*i + 1];
                    actual_data[3] = value[4*i];

                    int num; // add all the data from the BLE packet into an integer
                    num = (0xFF & actual_data[0]);
                    num *= 256;
                    num += (0xFF & actual_data[1]);
                    num *= 256;
                    num += (0xFF & actual_data[2]);
                    num *= 256;
                    num += (0xFF & actual_data[3]);
                    if (num == 0)
                        continue; // just continue to the next joints

                    num /= 64; // remove the last 6 binary digits (and shift right 6 bits) and now we are left with the decimals for x,y,z

                    int pos_x, pos_y, pos_z; // 'positive' bits
                    int one_x, one_y, one_z; // 'one' bits
                    byte extras = (byte) (actual_data[3] % 64); // same as AND operation with 0b00111111 (the decimal number 63)

                    pos_x = extras & 0b00000001;
                    one_x = extras & 0b00000010;
                    pos_y = extras & 0b00000100;
                    one_y = extras & 0b00001000;
                    pos_z = extras & 0b00010000;
                    one_z = extras & 0b00100000;

                    float x_dec, y_dec, z_dec; // decimal part of coordinates (and then they are set to the actual coordinate)
                    x_dec = (num  % 100)/100f;
                    num /= 100;
                    y_dec = (num  % 100)/100f;
                    num /= 100;
                    z_dec = (num  % 100)/100f; // doesn't really need mod 100, but it looks more consistent and easier to read

                    if (one_x != 0)
                        x_dec += 1f;
                    if (one_y != 0)
                        y_dec += 1f;
                    if (one_z != 0)
                        z_dec += 1f;
                    if (pos_x != 0)
                        x_dec *= -1f;
                    if (pos_y != 0)
                        y_dec *= -1f;
                    if (pos_z != 0)
                        z_dec *= -1f;

                    joints[i][CORD_X] = x_dec;
                    joints[i][CORD_Y] = y_dec;
                    joints[i][CORD_Z] = z_dec;
                }
            } else {
                Log.debug("received unknown characteristic UUID: " + characteristic.getUuid());
                return;
            }

            runOnUiThread( // in order to be able to change a View, it must be done in this UI Thread
                new Runnable() {
                    @Override
                    public void run() {
                        if (mp.isPlaying() || mp_error.isPlaying()) {
                            ; // if the sound is playing, wait until it's stopped (it's a short sound)
                        }
                        else if (started) {
                            if (user_facing_camera()) { // facing camera (~16° tolerance)
                                if (arm_is_straight()) { // arm is straight (~19° tolerance, because arm joints are never seen as straight from camera)
                                    if (step == 3) {
                                        did_or_f_ex_1_text.setTextColor(Color.BLACK);
                                        did_or_f_ex_1_text.setText("User may continue the exercise.");
                                        down_up_down_txt.setText("⚫\n⚫\n⚫");
                                        step = 0;
                                    }

                                    if (arm_down()) { // arm down
                                        Log.debug("Arm down! step : " + step);

                                        if (step == 0) { // exercise just started
                                            picked_image.setImageResource(R.drawable.normal_1);
                                            did_or_f_ex_1_text.setTextColor(Color.BLACK);
                                            did_or_f_ex_1_text.setText("User may continue the exercise.");
                                            down_up_down_txt.setText("✅ DOWN\n⚫\n⚫");
                                            step = 1;
                                        }

                                        if (arm_classification == Classif.OTHER || arm_classification == Classif.NONE) {
                                            Log.debug("\t...now it is! step : " + step);

                                            if (step == 1) { // arm was down
                                                Log.debug("show error now");
                                                picked_image.setImageResource(R.drawable.incorrect_1);
                                                did_or_f_ex_1_text.setTextColor(Color.parseColor(redColorString));
                                                did_or_f_ex_1_text.setText("❌ Arm wasn't raised at 90°.");
                                                mp_error.start();
                                                down_up_down_txt.setText("⚫\n⚫\n⚫");
                                                step = 0;
                                            }
                                            else if (step == 2) { // arm was up
                                                step = 3;
                                                picked_image.setImageResource(R.drawable.correct_1);
                                                did_or_f_ex_1_text.setTextColor(Color.GREEN);
                                                did_or_f_ex_1_text.setText("✅ Exercise cycle completed!");
                                                reps++;
                                                reps_num.setText("" + reps);
                                                down_up_down_txt.setText("✅ DOWN\n✅ UP\n✅ DOWN");
                                                mp.start();
                                            }
                                        }

                                        arm_classification = Classif.DOWN;
                                    }
                                    else if (arm_right()) { // arm right
                                        Log.debug("Arm right! step : " + step);
                                        if (arm_classification == Classif.OTHER || arm_classification == Classif.NONE) {
                                            Log.debug("\t... now it is! step : " + step);
                                            if (step == 1) { // was down
                                                picked_image.setImageResource(R.drawable.normal_1);
                                                did_or_f_ex_1_text.setTextColor(Color.BLACK);
                                                did_or_f_ex_1_text.setText("User may continue the exercise.");
                                                down_up_down_txt.setText("✅ DOWN\n✅ UP\n⚫");
                                                step = 2 ;
                                            }
                                            else if (step == 0) { // just started (wasnt even down)
                                                picked_image.setImageResource(R.drawable.incorrect_1);
                                                down_up_down_txt.setText("⚫\n⚫\n⚫");
                                                did_or_f_ex_1_text.setTextColor(Color.parseColor(redColorString));
                                                did_or_f_ex_1_text.setText("❌ Exercise must start with arm parallel to body.");
                                                mp_error.start();
                                                // restart exercise classifying etc.
                                            }
                                        }
                                        arm_classification = Classif.RIGHT;
                                    }
                                    else { // not down nor right
                                        Log.debug("Arm somewhere in the middle. step : " + step);
                                        picked_image.setImageResource(R.drawable.normal_1);
                                        arm_classification = Classif.OTHER;
                                        did_or_f_ex_1_text.setTextColor(Color.BLACK);
                                        did_or_f_ex_1_text.setText("User may continue the exercise.");
                                    }
                                }
                                else { // arm is not straight
                                    down_up_down_txt.setText("⚫\n⚫\n⚫");
                                    picked_image.setImageResource(R.drawable.incorrect_1);
                                    did_or_f_ex_1_text.setTextColor(Color.parseColor(redColorString));
                                    did_or_f_ex_1_text.setText("❌ User is facing camera but the arm is bent.");
//                                    mp_error.start();
                                    step = 0;
                                    // restart exercise classifying etc.
                                }
                            }
                            else { // user not facing camera
                                down_up_down_txt.setText("⚫\n⚫\n⚫");
                                picked_image.setImageResource(R.drawable.incorrect_1);
                                did_or_f_ex_1_text.setTextColor(Color.parseColor(redColorString));
                                did_or_f_ex_1_text.setText("❌ User is not facing the camera.");
//                                mp_error.start();
                                step = 0;
                                // restart exercise classifying etc.
                            }
                        } // else, we havent selected an image and we don't need to check anything about the joints
                    }
                }
            );
        }
    };

    /* -------------------------------------------------------------------------------- BLE profile class -------------------------------------------------------------------------------- */
    private static class Profile {

        /* Service UUID */
        public static UUID JointsServiceUUID = UUID.fromString("00000001-0000-1000-7000-123123123123");

        public static UUID arm = UUID.fromString("00000006-0000-1000-7000-123123123123");

        /**
         * Return a configured {@link BluetoothGattService} instance for the Current Service.
         */
        public static BluetoothGattService customJointsService() {
            Log.debug("Creating a gatt service according to custom joints service...");
            BluetoothGattService service = new BluetoothGattService(JointsServiceUUID,
                    BluetoothGattService.SERVICE_TYPE_PRIMARY);


            // Collar characteristic
            BluetoothGattCharacteristic whole_arm = new BluetoothGattCharacteristic(arm,
                    BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE,
                    BluetoothGattCharacteristic.PERMISSION_READ | BluetoothGattCharacteristic.PERMISSION_WRITE);

            service.addCharacteristic(whole_arm);

            Log.debug("\tcreated and added all characteristics to the service");

            return service;
        }
    }


    /* -------------------------------------------------- Activity specific methods -------------------------------------------------- */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.debug("this GameActivity onCreated...");

        setContentView(R.layout.my_layout);
        did_or_f_ex_1_text = findViewById(R.id.stepstext);
        down_up_down_txt = findViewById(R.id.down_up_down);
        reps_num = findViewById(R.id.reps_number_text);
        img1 = findViewById(R.id.imageButton);
        img2 = findViewById(R.id.imageButton2);
        restartbt = findViewById(R.id.button);
        startbt = findViewById(R.id.button3);
        press_start_to_text = findViewById(R.id.textView);
        showreps_text = findViewById(R.id.show_reps_text);

        down_up_down_txt.setText("⚫\n⚫\n⚫");
        down_up_down_txt.setTextColor(Color.GREEN);

        mp = MediaPlayer.create(context, R.raw.positive);
        mp_error = MediaPlayer.create(context, R.raw.error_1);

        mBluetoothManager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter mBluetoothAdapter = mBluetoothManager.getAdapter();
        Log.debug("\tset the bluetooth manager and adapter...");

        // We can't continue without proper Bluetooth support
        if (mBluetoothAdapter == null) {
            Log.debug("\tmyError: Bluetooth is not available");
            Log.debug("Bluetooth is not available");
        }
        if (!this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Log.debug("\tmyError: BLE is not supported");
            Log.debug("BLE is not supported");
        }

        // Register for system Bluetooth events
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        this.registerReceiver(mBluetoothReceiver, filter);
        Log.debug("\tregister bt receiver to an ActionStateChanged filter...");

        if (!mBluetoothAdapter.isEnabled()) {
            Log.debug("\tEnabling bt adapter, starting advertising and server");
            mBluetoothAdapter.enable();
        }

        startAdvertising();
        startServer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.debug("this GameActivity onDestroyed...");
        BluetoothAdapter bluetoothAdapter = mBluetoothManager.getAdapter();
        if (bluetoothAdapter.isEnabled()) {
            Log.debug("\tbt adapter is enabled, so stopping both server and advertising...");
            stopServer();
            stopAdvertising();
        }

        Log.debug("\tunregister bt receiver...");
        this.unregisterReceiver(mBluetoothReceiver);
    }

}





